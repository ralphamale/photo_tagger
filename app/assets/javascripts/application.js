// This is a manifest file that'll be compiled into application.js,
// which will include all the files listed below.
//
// Any JavaScript/Coffee file within this directory,
// lib/assets/javascripts, vendor/assets/javascripts, or
// vendor/assets/javascripts of plugins, if any, can be referenced
// here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll
// appear at the bottom of the the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE
// PROCESSED, ANY BLANK LINE SHOULD GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.serializeJSON
//= require underscore
//
//= require_tree ./models
//= require_tree ../templates
//= require_tree ./views
//
//= require_tree .


PT.initialize = function(current_user_id) {




  PT.showPhotosIndex(current_user_id);





  // might update callback later

  // wait a sec
  // console.log(PT.Photo.all);
  // alert(PT.Photo.all.length);
  // var photoListView = new PT.PhotosListView();
  // var $el = photoListView.render();
  // $('.content').append($el);
};

PT.showPhotosIndex = function(current_user_id) {
  var photosListView = new PT.PhotosListView();
  var photoFormView = new PT.PhotoFormView();

  var $el = photoFormView.render();
  $('#content').append($el);

  PT.Photo.fetchByUserId(current_user_id, function() {
    // console.log(PT.Photo.all);
    // alert(PT.Photo.all.length);
    var $el = photosListView.render();
    $('#content').append($el);



  });

}

PT.showPhotoDetail = function(photo) {
  $('#content').empty();

  var photoDetail = new PT.PhotoDetailView(photo);
  photoDetail.render();
};