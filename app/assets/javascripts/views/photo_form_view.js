(function() {

  var PT = window.PT = (window.PT || {});

  var PhotoFormView = PT.PhotoFormView = function() {
    this.$el = $('<div>')

    this.$el.on('submit', 'form', function(event) {
      event.preventDefault();
      var formData = $(this).serializeJSON()["photo"];

      console.log(formData);
      var newPicture = new PT.Photo(formData);
      console.log(newPicture);
      newPicture.create();
    })

    var that = this;




  };

  PhotoFormView.prototype.render = function() {

    var renderedContent = JST["photo_form"];

    this.$el.html(renderedContent);
    $('#content').append(this.$el);
    return this;

  };

  PhotoFormView.prototype.submit = function(event) {

  }

})();