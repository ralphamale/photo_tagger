(function() {
  var PT = window.PT = (window.PT || {})
  var TagSelectView = PT.TagSelectView = function(photo,event) {
    this.$el = $('<div>');

    var $photo = $(event.currentTarget);
    var $tag = $photo.find(".tag");

    $photo.addClass("is-tagged");

    // We want x and y in percentages relative to the
    // top left corner of the photo.

    var x = ((event.pageX - $photo.offset().left) / $photo.outerWidth()) * 100;
    var y = ((event.pageY - $photo.offset().top) / $photo.outerHeight()) * 100;

    // Set the inline style of the tag.
    // The rest is taken care for by the css.

    $tag.css({
      "left": x + "%",
      "top": y + "%"
    });

  }

  TagSelectView.prototype.render = function() {
    var $el = $('<div class="photo-tag">');
    return $el;
  }

})();