(function() {
  var PT = window.PT = (window.PT || {});

  var PhotoDetailView = PT.PhotoDetailView = function(photo) {
    this.$el = $('div');
    this.photo = photo;
    var that = this;
    $(document).ready(function(){

      $("#content").on("click", '.photo', function(event){
        var tagSelectView = new PT.TagSelectView(that.photo, event);

        var $el = tagSelectView.render();

        $('#content').prepend($el);

        var ptz = new PT.PhotoTagging({photo_id: 37, user_id: 1, x_pos: 200, y_pos: 200});
        ptz.create();

        // t.integer  "photo_id",   :null => false
        // t.integer  "user_id",    :null => false
        // t.integer  "x_pos",      :null => false
        // t.integer  "y_pos",      :null => false


      });
    });

  };

  PhotoDetailView.prototype.render = function() {
    var that = this;
    var renderedContent = JST["photo_detail"]({
      photo: that.photo
    });

    that.$el.html(renderedContent);
    $('#content').html(that.$el);
    return that;
  };



})();