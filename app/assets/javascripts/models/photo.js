(function() {

  var PT = window.PT = (window.PT || {})


  var Photo = PT.Photo = function(attributes) {
    this.attributes = _.extend({}, attributes);
  }

  Photo._events = {};

  //event1 : callback1 ...

  // Photo._events.add = undefined.

  Photo.on = function(eventName, callback) {
    if (Photo._events[eventName] === undefined) {
      Photo._events[eventName] = [];
    }
    Photo._events[eventName].push(callback);
  };

  Photo.trigger = function(eventName) {
    var allCallbacks = Photo._events[eventName];
    allCallbacks.forEach(function(callback) {
      //console.log(callback);
      callback();
      //trouble shoot:
      //log objs to console.log
      //render params at json back.
      //dump params to chrome console
    });
  };

  Photo.all = [];

  Photo.find = function(id) {
    var found = null;
    Photo.all.forEach(function(curPic) {
      if (curPic.get('id') === id) {
        found= curPic;
      }
    })

    return found;
  }

  Photo.prototype.get = function(attr_name) {
    return this.attributes[attr_name]; //close attention
  };

  Photo.prototype.set = function(attr_name, val) {
    this.attributes[attr_name] = val;
  };

  Photo.prototype.add = function() {
    alert("Add me!");
  }

  Photo.prototype.create = function(callback) {
    if (this.id) {
      alert("Photo already exists");
    }

    Photo.all.push(this);
    //maybe put in callback ?



    $.ajax({
      url: '/api/photos',
      method: 'POST',
      dataType: 'json',
      data: { photo: this.attributes }, /// FUCK!!!!!!!!!!!
      // PARAMS.

      success: function(resp) {
        //Photo.all.push(resp);
        Photo.trigger("add");
      }
      // maybe execute callback?
    });
  };

  Photo.prototype.save = function(callback) {
    var photo = this;

    if (this.id) {
      $.ajax({
        url: '/api/photos',
        // do we specify method?
        method: 'PUT',
        dataType: 'json',
        success: function(response) {
          _.extend(photo.attributes, response);
        }
      });
      // sucess will return an photo object


    } else {
      // callback comes from where?
      this.create(callback);
    }
  };

  Photo.fetchByUserId = function(userId, callback) {

     $.ajax({
      url: '/api/users/' + userId + '/photos',
      dataType: 'json',
      // turn POJOs into array of photos
      success: function(userPhotoAttributes) {
        var userPhotos = userPhotoAttributes.map(function(userPhotoAttribute) {
          return new PT.Photo(userPhotoAttribute);
        })

        Photo.all = Photo.all.concat(userPhotos);

        if (callback) {
          callback(userPhotos);
        }

      }// function(response) {
//         Photo.all.push(response);
//         //does
//         alert(response);
//         console.log(response);
//       }

      //callback
    });

    // callback

    // var currentPhoto;




  };


})();